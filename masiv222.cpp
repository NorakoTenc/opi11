﻿#include<iostream>
#include<iomanip>
#include "windows.h"
#include"math.h"
using namespace std;
int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    const int i = 4;
    int mas[i][i];
    srand((int)time(NULL));
    for (int n = 0; n < i; n++)
    {
        for (int m = 0; m < i; m++)
        {
            mas[n][m] = rand() % 201 - 100;
            cout << setw(5) << mas[n][m];
        }
        cout << endl;
    }
    int max = mas[0][0];
    int maxn = 0, maxm = 0;
    int min = mas[0][0];
    for (int n = 0; n < i; n++)
    {
        for (int m = 1; m < i; m++)
        {
            if (abs(mas[n][m]) > abs(max))
            {
                max = mas[n][m];
                maxn = n;
                maxm = m;
            }
            if (abs(mas[n][m]) < abs(min))
            {
                min = mas[n][m];
            }

        }
    }
    cout <<"Max = "<< max << endl;
    cout <<"Min = "<< min << endl;
    mas[maxn][maxm] = min;
    for (int n = 0; n < i; n++)
    {
        for (int m = 0; m < i; m++)
        {
            cout << setw(5) << mas[n][m];
        }
        cout << endl;
    }
}